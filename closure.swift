func logger(closure: () -> Void) {
	print("Running logger")
	closure()
	print("Done")
}

logger {
	print("This function is closure")
}
